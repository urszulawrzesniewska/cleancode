package pl.codementors.cleancode;

import java.util.List;

public interface MachineRepository {

    void add(Machine machine);

    boolean remove(Machine machine);

    List<Machine> getall();

    List<Machine> findBy(MachineType machineType);

    List<Machine> findByName(String machineName);
}
