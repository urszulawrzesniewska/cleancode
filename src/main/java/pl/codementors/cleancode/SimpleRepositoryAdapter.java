package pl.codementors.cleancode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepositoryAdapter implements MachineRepository{

    private SimpleRepository simpleRepository;

    public SimpleRepositoryAdapter (SimpleRepository simpleRepository) {
        this.simpleRepository = simpleRepository;
    }
    @Override
    public void add(Machine machine) {
    simpleRepository.add(machine);
    }

    @Override
    public boolean remove(Machine machine) {
        return simpleRepository.remove(machine);
    }

    @Override
    public List<Machine> getall() {
        List<Machine> machines = new ArrayList<>();
        for (MachineType machineType: MachineType.values()){
            machines.addAll(simpleRepository.find(machineType));
        }
        return machines;
    }

    @Override
    public List<Machine> findBy(MachineType machineType) {
        return simpleRepository.find(machineType);
    }

    @Override
    public List<Machine> findByName(String machineName) {
        return getall().stream()
                .filter(item -> item.getName()
                        .equals(machineName)).collect(Collectors.toList());
    }
}
