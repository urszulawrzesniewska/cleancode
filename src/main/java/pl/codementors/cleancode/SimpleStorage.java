package pl.codementors.cleancode;

import java.util.List;
import java.util.ArrayList;

public class SimpleStorage implements Storage {

    private List<Machine> dataStorage = new ArrayList<>();

    @Override
    public void add(Machine machine) {
        if (dataStorage != null) {
            dataStorage.add(machine);
        }
        else {
            throw new RuntimeException();
        }
    }

    @Override
    public boolean remove(Machine machine) {
        return dataStorage.remove(machine);
    }
    @Override
    public List<Machine> getall() {
        List<Machine> copyStorage = new ArrayList<>();
        copyStorage.addAll(dataStorage);
        return copyStorage;
    }
}
