package pl.codementors.cleancode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepository {
    private SimpleStorage storage;

    public SimpleRepository(SimpleStorage storage) {
        this.storage = storage;
    }
    void add (Machine machine) {
        storage.add(machine);
    }

    boolean remove (Machine machine) {
    return storage.remove(machine);
    }
    List<Machine> find (MachineType machineType) {
        List<Machine> result = new ArrayList<>();

        for(Machine machine: storage.getall()){
            if(machine.getType() == machineType){
                result.add(machine);
            }
        }

        return result;

//        return storage.getall().stream()
 //               .filter(machine -> machine.getType() == machineType)
  //              .collect(Collectors.toList());
    }

}
