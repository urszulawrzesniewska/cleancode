package pl.codementors.cleancode;

import java.util.List;

public interface Storage {
    void add (Machine machine);
    boolean remove (Machine machine);
    List<Machine> getall();
}
