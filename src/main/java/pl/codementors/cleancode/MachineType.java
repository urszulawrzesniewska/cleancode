package pl.codementors.cleancode;

public enum MachineType {

    STANDARD, EXTENDED
}
