package pl.codementors.cleancode;

import java.util.List;

public interface MachineReader {

    List<Machine> read (String fileName);
}
