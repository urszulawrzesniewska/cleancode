package pl.codementors.cleancode;

import java.util.List;

public class MachineService {

    private MachineRepository repository;

    public MachineService(MachineRepository repository) {
        this.repository = repository;
    }

    public boolean addIfNotExists(Machine machine) {
        if (repository.getall().stream().anyMatch(machine1 -> machine == machine1)) {
            return false;
        } else {
            repository.add(machine);
            return true;
        }
    }

    public boolean deleteByName(String machineName) {
        List<Machine> machines = repository.findByName(machineName);
        for (Machine machine : machines) {
            repository.remove(machine);
        }
        return machines.size() > 0;
    }

    public boolean deleteByType(MachineType machineType) {
        List<Machine> machines = repository.findBy(machineType);
        for (Machine machine : machines) {
            repository.remove(machine);
        }
        return machines.size() > 0;
    }

    public boolean remove(Machine machine){
        return repository.remove(machine);
    }

    public List<Machine> findWithName(String machineName) {
        return repository.findByName(machineName);
    }

    public List<Machine> findWithType(MachineType machineType) {
        return repository.findBy(machineType);
    }

    public List<Machine> findAll() {
        return repository.getall();
    }
}
