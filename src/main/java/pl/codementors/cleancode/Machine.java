package pl.codementors.cleancode;

import java.util.Objects;

public class Machine {
    private String name;
    private Size size;
    private MachineType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public MachineType getType() {
        return type;
    }

    public void setType(MachineType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Machine machine = (Machine) o;
        return Objects.equals(name, machine.name) &&
                Objects.equals(size, machine.size) &&
                type == machine.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size, type);
    }

    @Override
    public String toString() {
        return "Machine{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", type=" + type +
                '}';

    }
}
