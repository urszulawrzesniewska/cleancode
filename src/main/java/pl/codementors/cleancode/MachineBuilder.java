package pl.codementors.cleancode;

import static pl.codementors.cleancode.Size.*;

public class MachineBuilder {
    private String name;
    private MachineType machineType;
    private int height;
    private int width;

    public MachineBuilder withName(String name) {
        this.name = name;
        return this;
    }
    public MachineBuilder withType(MachineType type) {
        this.machineType = type;
        return this;
    }

    public MachineBuilder withHeight (int height) {
        this.height = height;
        return this;
    }
    public MachineBuilder withWidth (int width) {

        this.width = width;
        return this;
    }
    public Machine build() {
        Machine p = new Machine();
        p.setName(this.name);
        p.setType((this.machineType));
        Size size = new Size();
        size.setHeight(this.height);
        size.setWidth(this.width);
        p.setSize(size);
        return p;
    }
}
