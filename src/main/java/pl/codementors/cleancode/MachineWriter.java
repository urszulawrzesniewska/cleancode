package pl.codementors.cleancode;

import java.util.List;

public interface MachineWriter {

    void write (String fileName, List<Machine> listMachine);
}
