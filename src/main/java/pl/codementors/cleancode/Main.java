package pl.codementors.cleancode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

public class Main {
    final static SimpleRepository simpleRepository = new SimpleRepository(new SimpleStorage());
    final static MachineRepository machineRepository = new SimpleRepositoryAdapter(simpleRepository);
    final static MachineService machineService = new MachineService(machineRepository);
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        mainLoop:
        while (true) {
            System.out.println("Choose action: ");
            System.out.println("1. Add machine");
            System.out.println("2. Remove machine");
            System.out.println("3. Show machine list");
            System.out.println("4. Find machine by its name");
            System.out.println("5. Find machine by its type");
            System.out.println("6. Read machine list");
            System.out.println("7. Print machine list");
            System.out.println("8. Quit");


            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            if (choice.equals("1")) {
                System.out.println("Add machine");
                Machine selectedMachine = getSelectedMachine(scanner);
                machineService.addIfNotExists(selectedMachine);
            }
            if (choice.equals("2")) {

                System.out.println("Remove machine");
                Machine machine = getSelectedMachine(scanner);
                machineService.remove(machine);

            }
            if (choice.equals("3")) {
                System.out.println(machineService.findAll());
            }
            if (choice.equals("4")) {
                System.out.println("Find machine by name");
                System.out.println("Please enter machine name");
                choice = scanner.nextLine();

                final List<Machine> machines = machineService.findWithName(choice);
                System.out.println(machines);
            }

            if (choice.equals("5")) {
                System.out.println("Find machine by type");
                System.out.println("Please enter machine type");
                choice = scanner.nextLine();
                if (choice.equals("STANDARD")) {
                    machineService.findWithType(MachineType.STANDARD).forEach(System.out::println);
                } else {
                    machineService.findWithType(MachineType.EXTENDED).forEach(System.out::println);
                }
            }
            if (choice.equals("6")) {




            }

            if (choice.equals("7")) {




            }
            if (choice.equals("8")) {
                break mainLoop;
            }
        }
    }

    private static Machine getSelectedMachine(Scanner scanner) {
        String choice;
        MachineBuilder machineBuilder = new MachineBuilder();
        System.out.println("Provide machine name");
        choice = scanner.nextLine();
        machineBuilder.withName(choice);
        System.out.println("Provide machine type");
        choice = scanner.nextLine();
        machineBuilder.withType(MachineType.valueOf(choice.toUpperCase()));
        System.out.println("Provide height");
        choice = scanner.nextLine();
        machineBuilder.withHeight(Integer.parseInt(choice));
        System.out.println("Provide width");
        choice = scanner.nextLine();
        machineBuilder.withWidth(Integer.parseInt(choice));
        return machineBuilder.build();
    }
}

